INTRODUCTION
------------

The Search API Emoji module adds a processor for Search API indexes, which converts
emoji characters into their Unicode text equivalents. This makes them
searchable using both the original character, or words that appear in the text
equivalent.


REQUIREMENTS
------------

You must be using Drupal core 7.50 or later, installed with UTF8MB4 support.
Configuration instructions can be found at https://www.drupal.org/node/2754539.

You also require the following modules:

 * Search API (https://drupal.org/project/search_api)
 * Libraries API (https://drupal.org/project/libraries)

And the following third-party libraries:

 * PHP Emoji (http://code.iamcal.com/php/emoji)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * Download the latest version of the PHP Emoji library, and copy the files
   to sites/all/libraries/php_emoji.
 * Enable the Search API Emoji processor on the relevant Search API search index,
   and set the fields which should be processed.


MAINTAINERS
-----------

Current maintainers:
 * Ben Kyriakou (ben.kyriakou) - https://drupal.org/user/1272308
