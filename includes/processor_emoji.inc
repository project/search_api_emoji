<?php

/**
 * @file
 * Contains SearchApiEmojiReplaceEmoji.
 */

/**
 * Processor for replacing emoji characters with text equivalents.
 */
class SearchApiEmojiReplaceEmoji extends SearchApiAbstractProcessor {

  protected static $phpEmoji = array();

  protected static $replacements = array();

  /**
   * {@inheritdoc}
   */
  public function __construct(SearchApiIndex $index, array $options = array()) {
    parent::__construct($index, $options);

    $library = libraries_load('php_emoji');

    if (!empty($library['loaded'])) {
      self::$phpEmoji = array_filter($GLOBALS['emoji_maps']['names'], 'strlen');
    }

    foreach (self::$phpEmoji as $charcode => $charname) {
      self::$replacements['spaces'][$charcode] = " $charcode ";
      self::$replacements['text_equivalents'][$charcode] = " $charname ";
    }
  }

  /**
   * Calls processKeys() for the keys and processFilters() for the filters.
   */
  public function preprocessSearchQuery(SearchApiQuery $query) {
    $keys = &$query->getKeys();
    $this->preprocessKeys($keys);
    $this->processKeys($keys);
    $filter = $query->getFilter();
    $filters = &$filter->getFilters();
    $this->processFilters($filters);
  }

  /**
   * Method for preprocessing Emojis from search keys.
   *
   * For non-array keys this does nothing, as the aim is to flatten emojis into
   * existing arrays.
   */
  protected function preprocessKeys(&$keys) {
    if (is_array($keys)) {
      $new_keys = array();

      foreach ($keys as $key => $v) {
        if (element_child($key)) {
          if (!$v && !is_numeric($v)) {
            continue;
          }

          if (is_array($v)) {
            $this->preprocessKeys($v);
          }
          else {
            $this->preprocessKey($v);
            $v = array_filter(explode(' ', $v));
            $new_keys = array_merge($new_keys, $v);
          }
        }
        else {
          $new_keys[$key] = $v;
        }
      }

      $keys = $new_keys;
    }
    else {
      $this->preprocessKey($keys);
    }
  }

  /**
   * Add spaces around Emoji characters before further processing.
   */
  protected function preprocessKey(&$value) {
    $this->spaceEmoji($value);
  }

  /**
   * Callback to process a single value.
   *
   * This will return an altered string, replacing emoji with text equivalents.
   * If the input is not a string, the original value will be kept.
   *
   * @param mixed $value
   *   The original search value.
   */
  protected function process(&$value) {
    if (is_string($value)) {
      $this->replaceEmoji($value);
    }
  }

  /**
   * Replace Emoji characters with text equivalents.
   *
   * strtr() is used over str_replace() or regex since this seems to give fairly
   * performant and constant time transformations for both long and short
   * strings. It must be used with the array argument to work correctly with
   * multibyte strings.
   */
  protected function spaceEmoji(&$value) {
    $value = trim(strtr($value, self::$replacements['spaces']));
  }

  /**
   * Replace Emoji characters with text equivalents.
   */
  protected function replaceEmoji(&$value) {
    $value = strtr($value, self::$replacements['text_equivalents']);

    while (strpos($value, '  ') !== FALSE) {
      $value = strtr($value, array('  ' => ' '));
    }

    $value = trim($value);
  }

}
